# Gamindex

> This project is a fork of [github.com/OlehZanevych/deanery-202](https://github.com/OlehZanevych/deanery-2023)

Gamindex states for `Game` and `Index`. 
It is an index-service for video games.

The main entity is `Game` with following properties:
- `Title`
- `Series`
- `Genre`
- `Publisher`
- `Release Date`
- `Developer`

The service runs on port `8080`. 
The Swagger UI is available at <http://localhost:8080/swagger-ui/index.html>
(the service redirects to this endpoint automatically from the root).
