package com.nebelfox.edu.web.gamindex.dto.game;

import lombok.Data;

@Data
public class GameDto extends BaseGameDto {
    private Long id;
}
