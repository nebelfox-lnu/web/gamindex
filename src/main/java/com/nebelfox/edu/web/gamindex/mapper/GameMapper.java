package com.nebelfox.edu.web.gamindex.mapper;

import com.nebelfox.edu.web.gamindex.dto.game.BaseGameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GameDto;
import com.nebelfox.edu.web.gamindex.entity.GameEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GameMapper {
    GameEntity toEntity(BaseGameDto gameDto);
    GameDto toDto(GameEntity gameEntity);
    List<GameDto> toDtoList(List<GameEntity> gameEntities);
}
