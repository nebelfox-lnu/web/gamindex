package com.nebelfox.edu.web.gamindex.entity.projection;

public interface UserAuthDataProjection {
    String getUsername();
    boolean getIsAdmin();
    String getPasswordHash();
}