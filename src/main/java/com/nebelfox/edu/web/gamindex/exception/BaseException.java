package com.nebelfox.edu.web.gamindex.exception;

public class BaseException extends RuntimeException {
    public BaseException(String message) {
        super(message);
    }
}
