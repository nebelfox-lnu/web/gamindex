package com.nebelfox.edu.web.gamindex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamindexApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamindexApplication.class, args);
	}

}
