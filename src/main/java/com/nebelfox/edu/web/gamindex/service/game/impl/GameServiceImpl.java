package com.nebelfox.edu.web.gamindex.service.game.impl;

import com.nebelfox.edu.web.gamindex.dto.filter.GameFilter;
import com.nebelfox.edu.web.gamindex.dto.game.BaseGameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GamePatch;
import com.nebelfox.edu.web.gamindex.entity.GameEntity;
import com.nebelfox.edu.web.gamindex.exception.BadRequestException;
import com.nebelfox.edu.web.gamindex.repository.game.GameRepository;
import com.nebelfox.edu.web.gamindex.service.game.GameService;
import lombok.AllArgsConstructor;
import com.nebelfox.edu.web.gamindex.mapper.GameMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    private final GameMapper gameMapper;

    @Override
    public GameDto create(BaseGameDto game) {
        GameEntity gameEntity = gameMapper.toEntity(game);
        GameEntity createdGameEntity = gameRepository.create(gameEntity);
        return gameMapper.toDto(createdGameEntity);
    }

    @Override
    public List<GameDto> findAll(GameFilter filter,
                                 Optional<Integer> limit,
                                 Optional<Integer> offset) {
        List<GameEntity> gameEntities = gameRepository.findAll(filter, limit, offset);
        return gameMapper.toDtoList(gameEntities);
    }

    @Override
    public int count(GameFilter filter) {
        return gameRepository.count(filter);
    }

    @Override
    public GameDto find(Long id) {
        GameEntity gameEntity = gameRepository.find(id);
        return gameMapper.toDto(gameEntity);
    }

    @Override
    public GameDto update(Long id, BaseGameDto gameDto) {
        GameEntity gameEntity = gameMapper.toEntity(gameDto);
        gameEntity.setId(id);

        return gameMapper.toDto(gameRepository.update(gameEntity));
    }

    @Override
    public GameDto patch(Long id, GamePatch gamePatch) {
        if (gamePatch.isEmpty()) {
            throw new BadRequestException("Game patch is empty!");
        }

        return gameMapper.toDto(gameRepository.patch(id, gamePatch));
    }

    @Override
    public void delete(Long id) {
        gameRepository.delete(id);
    }
}
