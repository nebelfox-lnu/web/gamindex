package com.nebelfox.edu.web.gamindex.repository.game.impl;

import com.nebelfox.edu.web.gamindex.dto.filter.GameFilter;
import com.nebelfox.edu.web.gamindex.dto.game.GamePatch;
import com.nebelfox.edu.web.gamindex.entity.GameEntity;
import com.nebelfox.edu.web.gamindex.exception.DataConflictException;
import lombok.AllArgsConstructor;
import com.nebelfox.edu.web.gamindex.exception.NotFoundException;
import com.nebelfox.edu.web.gamindex.repository.game.GameRepository;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
@AllArgsConstructor
public class GameRepositoryImpl implements GameRepository {

    private static final String INSERT_GAME_QUERY =
        """
        INSERT INTO game (
            title,
            series,
            genre,
            publisher,
            releaseDate,
            developer,
            director,
            mode,
            engine
        ) VALUES (
            :title,
            :series,
            :genre,
            :publisher,
            :releaseDate,
            :developer,
            :director,
            :mode,
            :engine
        )
        """;

    private static final String COUNT_GAMES_QUERY =
        """
        SELECT COUNT(*)
        FROM game
        """;

    private static final String SELECT_GAMES_QUERY =
        """
        SELECT
            id,
            title,
            series,
            genre,
            publisher,
            releaseDate,
            developer,
            director,
            mode,
            engine
        FROM game
        """;

    private static final String SELECT_GAME_BY_ID_QUERY =
        """
        SELECT
            id,
            title,
            series,
            genre,
            publisher,
            releaseDate,
            developer,
            director,
            mode,
            engine
        FROM game
        WHERE id = :id
        """;

    private static final String UPDATE_GAME_BY_ID_QUERY =
        """
        UPDATE game SET
            title = :title,
            series = :series,
            genre = :genre,
            publisher = :publisher,
            releaseDate = :releaseDate,
            developer = :developer,
            director = :director,
            mode = :mode,
            engine = :engine
        WHERE id = :id
        """;

    private static final String PATCH_GAME_BY_ID_QUERY_TEMPLATE =
        """
        UPDATE game SET
            %s
        WHERE id = :id
        """;

    private static final String DELETE_GAME_BY_ID_QUERY =
        """
        DELETE FROM game WHERE id = :id
        """;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final RowMapper<GameEntity> GAME_ROW_MAPPER = (rs, rowNum) -> {
        GameEntity entity = new GameEntity();

        entity.setId(rs.getObject("id", Long.class));
        entity.setTitle(rs.getString("title"));
        entity.setSeries(rs.getString("series"));
        entity.setGenre(rs.getString("genre"));
        entity.setPublisher(rs.getString("publisher"));
        entity.setReleaseDate(rs.getString("releaseDate"));
        entity.setDeveloper(rs.getString("developer"));
        entity.setDirector(rs.getString("director"));
        entity.setMode(rs.getString("mode"));
        entity.setEngine(rs.getString("engine"));

        return entity;
    };


    @Override
    public GameEntity create(GameEntity game) {
        SqlParameterSource sqlParameters = new MapSqlParameterSource()
            .addValue("title", game.getTitle())
            .addValue("series", game.getSeries())
            .addValue("genre", game.getGenre())
            .addValue("publisher", game.getPublisher())
            .addValue("releaseDate", game.getReleaseDate())
            .addValue("developer", game.getDeveloper())
            .addValue("director", game.getDirector())
            .addValue("mode", game.getMode())
            .addValue("engine", game.getEngine());

        KeyHolder keyHolder = new GeneratedKeyHolder();


        try {
            jdbcTemplate.update(INSERT_GAME_QUERY, sqlParameters, keyHolder);
        } catch (DuplicateKeyException e) {
            if (e
                .getCause()
                .getMessage()
                .contains("duplicate key value violates unique constraint \"game_title_key\"")) {
                throw new DataConflictException(String.format("Game with title \"%s\" already exists!",
                                                              game.getTitle()));
            }

            throw e;
        }

        Long id = (Long) keyHolder.getKeys().get("id");
        game.setId(id);

        return game;
    }

    @Override
    public List<GameEntity> findAll(GameFilter filter,
                                    Optional<Integer> limit,
                                    Optional<Integer> offset) {
        List<String> filters = new ArrayList<>();
        MapSqlParameterSource parameters = new MapSqlParameterSource();

        filter.toMap().forEach((name, value) -> {
            if (!name.startsWith("released")) {
                filters.add(name + "= :" + name);
                parameters.addValue(name, value);
            }
        });

        filter.getReleasedAfter().ifPresent(value -> {
            filters.add("releaseDate > :releasedAfter");
            parameters.addValue("releasedAfter", value);
        });


        filter.getReleasedBefore().ifPresent(value -> {
            filters.add("releaseDate < :releasedBefore");
            parameters.addValue("releasedBefore", value);
        });

        List<String> suffixes = new ArrayList<>();

        limit.ifPresent(value -> {
            suffixes.add(" LIMIT :limit");
            parameters.addValue("limit", value);
        });

        offset.ifPresent(value -> {
            suffixes.add(" OFFSET :offset");
            parameters.addValue("offset", value);
        });

        String where = filters.isEmpty() ? "" : " WHERE " + String.join(" AND ", filters);

        return jdbcTemplate.query(
            SELECT_GAMES_QUERY + where + String.join("", suffixes),
            parameters,
            GAME_ROW_MAPPER);
    }

    @Override
    public int count(GameFilter filter) {
        List<String> filters = new ArrayList<>();
        MapSqlParameterSource parameters = new MapSqlParameterSource();

        filter.toMap().forEach((name, value) -> {
            if (!name.startsWith("released")) {
                filters.add(name + "= :" + name);
                parameters.addValue(name, value);
            }
        });

        filter.getReleasedAfter().ifPresent(value -> {
            filters.add("releaseDate > :releasedAfter");
            parameters.addValue("releasedAfter", value);
        });


        filter.getReleasedBefore().ifPresent(value -> {
            filters.add("releaseDate < :releasedBefore");
            parameters.addValue("releasedBefore", value);
        });

        String querySuffix = filters.isEmpty() ? "" : "WHERE " + String.join(" AND ", filters);

        return jdbcTemplate.queryForObject(
            COUNT_GAMES_QUERY + querySuffix,
            parameters,
            Integer.class);
    }

    @Override
    public GameEntity find(Long id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_GAME_BY_ID_QUERY,
                                               new MapSqlParameterSource("id", id),
                                               GAME_ROW_MAPPER);
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException("Game with id " + id + " not found!");
        }
    }

    @Override
    public GameEntity update(GameEntity game) {

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(UPDATE_GAME_BY_ID_QUERY, new MapSqlParameterSource()
                .addValue("id", game.getId())
                .addValue("title", game.getTitle())
                .addValue("series", game.getSeries())
                .addValue("genre", game.getGenre())
                .addValue("publisher", game.getPublisher())
                .addValue("releaseDate", game.getReleaseDate())
                .addValue("developer", game.getDeveloper())
                .addValue("director", game.getDirector())
                .addValue("mode", game.getMode())
                .addValue("engine", game.getEngine()));
        } catch (DuplicateKeyException e) {
            if (e
                .getCause()
                .getMessage()
                .contains("duplicate key value violates unique constraint \"game_title_key\"")) {
                throw new DataConflictException(String.format("Game with title \"%s\" already exists!",
                                                              game.getTitle()));
            }

            throw e;
        }

        if (affectedRows == 0) {
            throw new NotFoundException("Game with id " + game.getId() + " not found!");
        }

        return game;
    }

    @Override
    public GameEntity patch(Long id, GamePatch gamePatch) {
        List<String> assignments = new ArrayList<>();
        MapSqlParameterSource parameters = new MapSqlParameterSource("id", id);

        new HashMap<String, Optional<String>>() {{
            put("title", gamePatch.getTitle());
            put("series", gamePatch.getSeries());
            put("genre", gamePatch.getGenre());
            put("publisher", gamePatch.getPublisher());
            put("releaseDate", gamePatch.getReleaseDate());
            put("developer", gamePatch.getDeveloper());
            put("director", gamePatch.getDirector());
            put("mode", gamePatch.getMode());
            put("engine", gamePatch.getEngine());
        }}.forEach((name, optional) -> {
            optional.ifPresent(value -> {
                assignments.add(name + " = :" + name);
                parameters.addValue(name, optional.get());
            });
        });

        String assignmentStr = String.join(", ", assignments);
        String query = String.format(PATCH_GAME_BY_ID_QUERY_TEMPLATE, assignmentStr);

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(query, parameters);
        } catch (DuplicateKeyException e) {
            if (e
                .getCause()
                .getMessage()
                .contains("duplicate key value violates unique constraint \"game_title_key\"")) {
                throw new DataConflictException(String.format("Game with title \"%s\" already exists!",
                                                              gamePatch.getTitle()));
            }

            throw e;
        }

        if (affectedRows == 0) {
            throw new NotFoundException("Game with id " + id + " not found!");
        }

        return find(id);
    }

    @Override
    public void delete(Long id) {
        int affectedRows = jdbcTemplate.update(DELETE_GAME_BY_ID_QUERY, new MapSqlParameterSource("id", id));

        if (affectedRows == 0) {
            throw new NotFoundException("Game with id " + id + " not found!");
        }
    }
}
