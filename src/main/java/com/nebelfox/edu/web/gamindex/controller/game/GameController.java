package com.nebelfox.edu.web.gamindex.controller.game;

import com.nebelfox.edu.web.gamindex.annotation.Auth;
import com.nebelfox.edu.web.gamindex.dto.filter.GameFilter;
import com.nebelfox.edu.web.gamindex.dto.game.GameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GamePatch;
import lombok.AllArgsConstructor;
import com.nebelfox.edu.web.gamindex.dto.game.BaseGameDto;
import com.nebelfox.edu.web.gamindex.service.game.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Auth
@RestController
@AllArgsConstructor
@RequestMapping("games")
public class GameController {

    private final GameService gameService;

    @Auth(isAdmin = true)
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public GameDto create(@RequestBody BaseGameDto game) {
        return gameService.create(game);
    }

    @GetMapping
    public List<GameDto> findAll(GameFilter filter,
                                 Optional<Integer> limit,
                                 Optional<Integer> offset) {
        return gameService.findAll(filter, limit, offset);
    }

    @GetMapping("count")
    public int count(GameFilter filter) {
        return gameService.count(filter);
    }

    @GetMapping("{id}")
    public GameDto find(@PathVariable Long id) {
        return gameService.find(id);
    }

    @Auth(isAdmin = true)
    @PutMapping("{id}")
    public GameDto update(@PathVariable Long id, @RequestBody BaseGameDto game) {
        return gameService.update(id, game);
    }

    @Auth(isAdmin = true)
    @PatchMapping("{id}")
    public GameDto patch(@PathVariable Long id, @RequestBody GamePatch gamePatch) {
        return gameService.patch(id, gamePatch);
    }

    @Auth(isAdmin = true)
    @DeleteMapping({"{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        gameService.delete(id);
    }
}
