package com.nebelfox.edu.web.gamindex.dto.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Optional;

@Data
public class GamePatch {
    private Optional<String> title = Optional.empty();
    private Optional<String> series = Optional.empty();
    private Optional<String> genre = Optional.empty();
    private Optional<String> publisher = Optional.empty();
    private Optional<String> releaseDate = Optional.empty();
    private Optional<String> developer = Optional.empty();
    private Optional<String> director = Optional.empty();
    private Optional<String> mode = Optional.empty();
    private Optional<String> engine = Optional.empty();

    public void setTitle(String title) {
        this.title = Optional.of(title);
    }

    public void setSeries(String series) {
        this.series = Optional.of(series);
    }

    public void setGenre(String genre) {
        this.genre = Optional.of(genre);
    }

    public void setPublisher(String publisher) {
        this.publisher = Optional.of(publisher);
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = Optional.of(releaseDate);
    }

    public void setDeveloper(String developer) {
        this.developer = Optional.of(developer);
    }

    public void setDirector(String director) {
        this.director = Optional.of(director);
    }

    public void setMode(String mode) {
        this.mode = Optional.of(mode);
    }

    public void setEngine(String engine) {
        this.engine = Optional.of(engine);
    }

    @JsonIgnore
    public Boolean isEmpty() {
        return this.title.isEmpty()
               && this.series.isEmpty()
               && this.genre.isEmpty()
               && this.publisher.isEmpty()
               && this.releaseDate.isEmpty()
               && this.developer.isEmpty()
               && this.director.isEmpty()
               && this.mode.isEmpty()
               && this.engine.isEmpty();
    }
}