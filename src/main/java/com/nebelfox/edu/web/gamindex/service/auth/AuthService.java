package com.nebelfox.edu.web.gamindex.service.auth;

import com.nebelfox.edu.web.gamindex.annotation.Auth;
import com.nebelfox.edu.web.gamindex.dto.user.UserCredentials;

public interface AuthService {
    void login(UserCredentials userCredentials);
    void verifyAuthority(Auth auth);
}
