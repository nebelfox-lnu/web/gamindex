package com.nebelfox.edu.web.gamindex.service.user.impl;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import com.nebelfox.edu.web.gamindex.dto.user.UserCreateDto;
import com.nebelfox.edu.web.gamindex.dto.user.UserDto;
import com.nebelfox.edu.web.gamindex.dto.user.UserUpdateDto;
import com.nebelfox.edu.web.gamindex.entity.user.UserEntity;
import com.nebelfox.edu.web.gamindex.exception.DataConflictException;
import com.nebelfox.edu.web.gamindex.exception.NotFoundException;
import com.nebelfox.edu.web.gamindex.mapper.UserMapper;
import com.nebelfox.edu.web.gamindex.repository.user.UserRepository;
import com.nebelfox.edu.web.gamindex.service.user.UserService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto create(UserCreateDto userDto) {
        UserEntity userEntity = userMapper.toEntity(userDto);

        String passwordHash = bCryptPasswordEncoder.encode(userDto.getPassword());
        userEntity.setPasswordHash(passwordHash);

        try {
            userEntity = userRepository.save(userEntity);
        } catch (DataIntegrityViolationException e) {
            Throwable cause = e.getCause();
            if (cause instanceof ConstraintViolationException) {
                if ("users_username_key".equals(((ConstraintViolationException) cause).getConstraintName())) {
                    String errorMessage = String.format("User with username '%s' already exists!", userEntity.getUsername());
                    throw new DataConflictException(errorMessage);
                }
            }

            throw e;
        }
        return userMapper.toDto(userEntity);
    }

    @Override
    public List<UserDto> findAll() {
        List<UserEntity> userEntities = userRepository.findAll();
        return userMapper.toDtoList(userEntities);
    }

    @Override
    public UserDto find(Long id) {
        UserEntity userEntity = userRepository.findById(id)
                                              .orElseThrow(() -> new NotFoundException("User not found!"));
        return userMapper.toDto(userEntity);
    }

    @Override
    public void update(Long id, UserUpdateDto userDto) {
        // Home task
    }

    @Override
    @Transactional
    public void delete(Long id) {
        int affectedRaws = userRepository.removeById(id);
        if (affectedRaws == 0) {
            throw new NotFoundException("User not found!");
        }
    }
}