package com.nebelfox.edu.web.gamindex.dto.game;

import lombok.Data;

@Data
public class BaseGameDto {
    private String title;
    private String series;
    private String genre;
    private String publisher;
    private String releaseDate;
    private String developer;
    private String director;
    private String mode;
    private String engine;
}
