package com.nebelfox.edu.web.gamindex.dto.filter;

import lombok.Data;

import java.util.HashMap;
import java.util.Optional;

@Data
public class GameFilter {
    private Optional<String> series = Optional.empty();
    private Optional<String> genre = Optional.empty();
    private Optional<String> publisher = Optional.empty();
    private Optional<String> releasedAfter = Optional.empty();
    private Optional<String> releasedBefore = Optional.empty();
    private Optional<String> developer = Optional.empty();
    private Optional<String> director = Optional.empty();
    private Optional<String> mode = Optional.empty();
    private Optional<String> engine = Optional.empty();

    public void setSeries(String series) {
        this.series = Optional.of(series);
    }

    public void setGenre(String genre) {
        this.genre = Optional.of(genre);
    }

    public void setPublisher(String publisher) {
        this.publisher = Optional.of(publisher);
    }

    public void setReleasedBefore(String releasedBefore) {
        this.releasedBefore = Optional.of(releasedBefore);
    }

    public void setReleasedAfter(String releasedAfter) {
        this.releasedAfter = Optional.of(releasedAfter);
    }

    public void setDeveloper(String developer) {
        this.developer = Optional.of(developer);
    }

    public void setDirector(String director) {
        this.director = Optional.of(director);
    }

    public void setMode(String mode) {
        this.mode = Optional.of(mode);
    }

    public void setEngine(String engine) {
        this.engine = Optional.of(engine);
    }

    private HashMap<String, Optional<String>> toOptionalMap() {
        return new HashMap<>() {{
            put("series", series);
            put("genre", genre);
            put("publisher", publisher);
            put("releasedAfter", releasedAfter);
            put("releasedBefore", releasedBefore);
            put("developer", developer);
            put("director", director);
            put("mode", mode);
            put("engine", engine);
        }};
    }

    public HashMap<String, String> toMap() {
        HashMap<String, String> map = new HashMap<>();
        toOptionalMap().forEach((name, optional) -> {
            optional.ifPresent(value -> {
                map.put(name, value);
            });
        });
        return map;
    }
}
