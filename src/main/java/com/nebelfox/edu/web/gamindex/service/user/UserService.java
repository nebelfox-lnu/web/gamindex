package com.nebelfox.edu.web.gamindex.service.user;

import com.nebelfox.edu.web.gamindex.dto.user.UserCreateDto;
import com.nebelfox.edu.web.gamindex.dto.user.UserDto;
import com.nebelfox.edu.web.gamindex.dto.user.UserUpdateDto;

import java.util.List;

public interface UserService {
    UserDto create(UserCreateDto userDto);
    List<UserDto> findAll();
    UserDto find(Long id);
    void update(Long id, UserUpdateDto userDto);
    void delete(Long id);
}
