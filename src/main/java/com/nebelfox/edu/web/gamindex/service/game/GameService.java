package com.nebelfox.edu.web.gamindex.service.game;

import com.nebelfox.edu.web.gamindex.dto.filter.GameFilter;
import com.nebelfox.edu.web.gamindex.dto.game.BaseGameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GameDto;
import com.nebelfox.edu.web.gamindex.dto.game.GamePatch;

import java.util.List;
import java.util.Optional;

public interface GameService {
    GameDto create(BaseGameDto game);
    List<GameDto> findAll(GameFilter filter, Optional<Integer> limit, Optional<Integer> offset);

    int count(GameFilter filter);
    GameDto find(Long id);
    GameDto update(Long id, BaseGameDto gameDto);
    GameDto patch(Long id, GamePatch patch);
    void delete(Long id);
}
