package com.nebelfox.edu.web.gamindex.mapper;


import com.nebelfox.edu.web.gamindex.dto.user.UserCreateDto;
import com.nebelfox.edu.web.gamindex.dto.user.UserDto;
import com.nebelfox.edu.web.gamindex.entity.user.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserEntity toEntity(UserCreateDto userDto);
    UserDto toDto(UserEntity userEntity);
    List<UserDto> toDtoList(List<UserEntity> userEntities);
}