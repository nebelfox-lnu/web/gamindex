package com.nebelfox.edu.web.gamindex.repository.game;

import com.nebelfox.edu.web.gamindex.dto.filter.GameFilter;
import com.nebelfox.edu.web.gamindex.dto.game.GamePatch;
import com.nebelfox.edu.web.gamindex.entity.GameEntity;

import java.util.List;
import java.util.Optional;

public interface GameRepository {
    GameEntity create(GameEntity game);
    List<GameEntity> findAll(GameFilter filter,
                             Optional<Integer> limit,
                             Optional<Integer> offset);

    int count(GameFilter filter);
    GameEntity find(Long id);
    GameEntity update(GameEntity game);
    GameEntity patch(Long id, GamePatch gamePatch);
    void delete(Long id);
}
