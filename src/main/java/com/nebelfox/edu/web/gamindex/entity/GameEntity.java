package com.nebelfox.edu.web.gamindex.entity;

import lombok.Data;

@Data
public class GameEntity {
    private Long id;
    private String title;
    private String series;
    private String genre;
    private String publisher;
    private String releaseDate;
    private String developer;
    private String director;
    private String mode;
    private String engine;
}
