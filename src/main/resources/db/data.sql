INSERT INTO game (id, title, series, genre, publisher, releaseDate, developer, director, mode, engine)
VALUES (1, 'Far Cry 1', 'Far Cry', 'First-person shooter', 'Ubisoft', '2004-03-26', 'Crytek', 'Cevat Yerli', 'single-player', 'CryEngine 1'),
       (2, 'Far Cry 2', 'Far Cry', 'First-person shooter', 'Ubisoft', '2008-10-24', 'Ubisoft Montreal', 'Clint Hocking', 'single-player', 'Dunia Engine'),
       (3, 'Far Cry 3', 'Far Cry', 'First-person shooter', 'Ubisoft', '2012-11-29', 'Ubisoft Montreal', 'Patrick Plourde', 'single-player', 'Dunia 2'),
       (4, 'Far Cry 4', 'Far Cry', 'First-person shooter', 'Ubisoft', '2014-11-18', 'Ubisoft Montreal', 'Alex Hutchinson', 'single-player', 'Dunia Engine'),
       (5, 'Far Cry 5', 'Far Cry', 'First-person shooter', 'Ubisoft', '2018-03-27', 'Ubisoft Montreal', 'Dan Hay', 'single-player', 'Dunia Engine'),
       (6, 'Grand Theft Auto: San Andreas', 'Grand Theft Auto', 'Action-adventure', 'Rockstar Games', '2004-10-26', 'Rockstar North', 'Obbe Vermeij', 'single-player', 'RenderWare'),
       (7, 'Grand Theft Auto IV', 'Grand Theft Auto', 'Action-adventure', 'Rockstar Games', '2008-11-03', 'Rockstar North', 'Leslie Benzies', 'single-player', 'RAGE'),
       (8, 'Grand Theft Auto V', 'Grand Theft Auto', 'Action-adventure', 'Rockstar Games', '2013-09-17', 'Rockstar North', 'Leslie Benzies', 'single-player', 'RAGE'),
       (9, 'Assassin''s Creed II', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2009-11-09', 'Ubisoft Montreal', 'Patrice Désilets', 'single-player', 'Anvil'),
       (10, 'Assassin''s Creed: Revelations', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2011-11-15', 'Ubisoft Montreal', 'Alexandre Amancio', 'single-player', 'Anvil'),
       (11, 'Assassin''s Creed IV: Black Flag', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2013-10-29', 'Ubisoft Montreal', 'Jean Guesdon', 'single-player', 'AnvilNext'),
       (12, 'Assassin''s Creed Rogue', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2014-11-11', 'Ubisoft Sofia', 'Mikhail Lozanov', 'single-player', 'AnvilNext'),
       (13, 'Assassin''s Creed Unity', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2014-11-14', 'Ubisoft Sofia', 'Alexandre Amancio', 'single-player', 'AnvilNext 2.0'),
       (14, 'Assassin''s Creed Syndicate', 'Assassin''s Creed', 'Action-adventure, stealth', 'Ubisoft', '2015-10-23', 'Ubisoft Quebec', 'Marc-Alexis Côté', 'single-player', 'AnvilNext 2.0'),
       (15, 'Saints Row', 'Saints Row', 'Action-adventure', 'THQ', '2006-09-01', 'Volition', 'Jacques Hennequet', 'single-player', 'Proprietary Engine'),
       (16, 'Saints Row 2', 'Saints Row', 'Action-adventure', 'THQ', '2008-10-17', 'Volition', 'Clint Ourso', 'single-player', 'Havok'),
       (17, 'Saints Row: The Third', 'Saints Row', 'Action-adventure', 'THQ', '2011-11-18', 'Volition', 'Scott Phillips', 'single-player', 'Havok'),
       (18, 'Saints Row IV', 'Saints Row', 'Action-adventure', 'Deep Silver', '2013-08-20', 'Volition', 'Jim Boone', 'single-player', 'Havoc'),
       (19, 'Minecraft', 'Minecraft', 'Sandbox, survival', 'Mojang Studios', '2011-11-18', 'Mojang Studios', 'Agnes Larsson', 'single-player', 'Lightweight Java Game Library'),
       (20, 'Archvale', 'Archvale', 'Action-adventure', 'Humble Games', '2021-12-02', 'idoz & phops', NULL, 'single-player', 'GameMaker');


SELECT setval('game_id_seq', 20);