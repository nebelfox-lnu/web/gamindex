DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TABLE game
(
    id      BIGSERIAL PRIMARY KEY,
    title   VARCHAR(64)  NOT NULL UNIQUE,
    series  VARCHAR(64)  NOT NULL,
    genre   VARCHAR(64)  NOT NULL,
    publisher   VARCHAR(64)  NOT NULL,
    releaseDate VARCHAR(10) NOT NULL,
    developer   TEXT,
    director TEXT,
    mode TEXT,
    engine TEXT
);

CREATE TABLE user
(
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(32) UNIQUE NOT NULL,
    password_hash VARCHAR(128) NOT NULL,
    is_admin BOOLEAN NOT NULL,
    first_name VARCHAR(64) NOT NULL,
    middle_name VARCHAR(64) NOT NULL,
    last_name VARCHAR(64) NOT NULL,
    phone VARCHAR(16) NOT NULL,
    email VARCHAR(32) NOT NULL,
    details TEXT
)